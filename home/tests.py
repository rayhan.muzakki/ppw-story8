from django.test import Client, TestCase, LiveServerTestCase
from selenium import webdriver
import time


# Create your tests here.

class unitTest(TestCase):

    def test_idxurl(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_indextemplate(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.driver.quit()

    def test_pressUp(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("up2").click()
        card= self.driver.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][1]")
        self.assertIn("headingTwo", card.get_attribute("id"))

    def test_pressDown(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("down1").click()
        card= self.driver.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][2]")
        self.assertIn("headingOne", card.get_attribute("id"))                        